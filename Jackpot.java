import java.util.Scanner;

public class Jackpot {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome player!");
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		int gamesWon = 0;
		boolean isContinue = true;
		
		while (isContinue == true){ //The user will keep playing games till they wish not to continue
			if (gameOver == true){ //If they wish to continue, we initialize the all the fields again
				gameOver = false;
				board = new Board();
				numOfTilesClosed = 0;
			}
			while (gameOver == false){
				System.out.println(board);
				boolean isGameOver = board.playATurn(); //made this variable to make things more clear
				if (isGameOver == true){
					gameOver = true;
				}
				else{
					numOfTilesClosed++;
				}
			}
			if (numOfTilesClosed >= 7){
				System.out.println("Lucky you! You have won the jackpot! WOOOO");
				gamesWon++;
			}
			else{
				System.out.println("Damn, you lost... better luck next time I guess.");
			}
	
			System.out.println("Do you wish to continue playing, player?");		
			System.out.println("Type N for no, Y for yes.");
			String userInput = scan.next();
			if (userInput.equals("Y")){
				System.out.println("Good choice, here comes another game!");
				System.out.println("");
			}
			else{
				System.out.println();
				System.out.println("That's understandable...");
				isContinue = false;
			}
		}
		System.out.println("Thank you for playing! You have won: " + gamesWon);
	}
}