import java.util.Random;

public class Die {
	private int faceValue;
	private Random randGen;
	
	public Die() {
		this.faceValue = 1;
		this.randGen = new Random();
	}
	
	public int getFaceValue() {
		return this.faceValue;
	}
	
	public void roll() {
		this.faceValue = randGen.nextInt(6)+1;
	}
	
	public String toString(){
		return this.faceValue + "";
	}
}