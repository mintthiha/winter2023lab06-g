public class Board{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board() {
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public String toString(){
		String string = "";
		
		for (int i = 0; i < tiles.length; i++){
			if (tiles[i] == false){
				string += i + 1 + " ";
			}
			else{
				string += "X ";
			}
		}
		return string + "";
	}
	
	public boolean playATurn(){
		die1.roll();
		die2.roll();
		System.out.println(die1);
		System.out.println(die2);
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		boolean isLost = false;
		if (this.tiles[sumOfDice - 1] == false){
			this.tiles[sumOfDice - 1] = true;
			System.out.println("Closing tile equal to sum: " + sumOfDice);
		}
		else if(this.tiles[die1.getFaceValue() - 1] == false){
			this.tiles[die1.getFaceValue() - 1] = true;
			System.out.println("Closing tile with the same value as die one: " + die1.getFaceValue());
		}
		else if(this.tiles[die2.getFaceValue() - 1] == false){
			this.tiles[die2.getFaceValue() - 1] = true;
			System.out.println("Closing tile with the same value as die two: " + die2.getFaceValue());
		}
		else{
			System.out.println("All the tiles for these values are already shut.");
			isLost = true;
		}
		return isLost;
	}
}